The "Upload Node Picture" module streamlines bulk image imports in Drupal, linking them to specific nodes by their IDs. Content managers can enrich content by uploading multiple images via an Excel sheet, eliminating manual insertion.

To use, structure your Excel file with node IDs and image URLs. The module supports both absolute and relative paths.

Access the upload form in Configuration > Upload Node Pictures. Define the node-image relation field and the image field.

This module boosts content management efficiency by simplifying image imports and includes error handling for smooth execution.

Empower your Drupal site with "Upload Node Picture" for effortless image management and content enrichment.

## Feature List for "Upload Node Picture" Module:

- Bulk Image Import: Upload multiple images at once via Excel sheet containing URLs associated with node IDs.
- Customizable Structure: Define the structure of the Excel file to match the fields in your content type, including the field for node ID and image URL.
- Node Field Relation: Establish a relationship between uploaded images and nodes using the specified field in the content type.
- Flexible URL Handling: Support for both absolute and relative URLs for image sources in the Excel sheet.
Administrative Interface: Access the module's functionality conveniently under the Configuration section.
User-friendly Form: Navigate to "/admin/config/upload-node-pictures/upload-pictures" to access the upload form.
- Streamlined Process: Simplify the image import process for content management tasks.
Enhanced Content Management: Empower content managers to enrich nodes with images efficiently.

## Post-Installation Instructions:

Congratulations on installing the "Upload Node Picture" module! Follow these steps to configure and start using the module effectively:

## Navigate to Configuration:
- Log in to your Drupal site as an administrator.
- Navigate to the Configuration section by clicking on the "Manage" menu at the top of the page, then selecting "Configuration."

## Access Upload Node Picture Settings:
-In the Configuration menu, locate and click on the "Upload Node Pictures" option. This will take you to the module's settings page.

## Define Excel File Structure:
- Ensure that you have a properly structured Excel file ready for import. The file should include columns for the node ID and the corresponding image URL. You can use absolute or relative URLs for image sources.

## Configure Node Field Relation:
- On the Upload Node Pictures settings page, specify the field in your content type that establishes the relation between nodes and images. This field will be used to associate uploaded images with their respective nodes.

## Select Image Field:
- Identify the field in your content type that contains the image itself. This field will be populated with the images imported from the Excel file.

## Navigate to Upload Form:
- Once the settings are configured, navigate to the upload form by accessing the URL "/admin/config/upload-node-pictures/upload-pictures". This is where you will upload your Excel file containing the image data.

## Upload Images:
- On the upload form, click on the "Choose File" button to select the Excel file from your computer. Then, click on the "Upload" button to initiate the import process.

## Review and Confirm:
- After uploading the file, review the imported data to ensure accuracy. The module will associate the images with their respective nodes based on the provided node IDs.

## Monitor Import Progress:
- Depending on the size of the Excel file and the number of images being imported, the process may take some time. Monitor the import progress and wait for the confirmation message.

## Verify Results:
- Once the import process is complete, verify that the images have been successfully associated with the corresponding nodes. You can do this by checking the nodes in your Drupal site's content management interface.