<?php

namespace Drupal\upload_node_pictures\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\entityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to upload pictures to nodes.
 */
class UploadNodePictureForm extends FormBase implements ContainerInjectionInterface {

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The module extension list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Constructs a new UploadNodePictureForm object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   The module extension list service.
   */
  public function __construct(MessengerInterface $messenger, EntityFieldManagerInterface $entityFieldManager, EntityTypeManagerInterface $entityTypeManager, FileSystemInterface $fileSystem, ModuleExtensionList $moduleExtensionList) {
    $this->messenger = $messenger;
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->moduleExtensionList = $moduleExtensionList;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('extension.list.module')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'upload_node_picture_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Add library.
    $form['#attached']['library'][] = 'upload_node_pictures/instructions';

    // Instructions.
    $form['documentation'] = [
      '#type' => 'inline_template',
      '#theme' => 'upload_node_pictures_instructions',
    ];

    // Content type field.
    $form['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content type'),
      '#options' => $this->getContentTypeOptions(),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::populateSelect',
        'event' => 'change',
        'wrapper' => 'field-relation',
        'progress' => [
          'type' => 'throbber',
          'message' => t('Checking fields...'),
        ],
      ],
    ];

    // Node relation field.
    $form['node_relation'] = [
      '#type' => 'select',
      '#title' => $this->t('Field of relation'),
      '#prefix' => '<div id="field-relation" class="field-relation">',
      '#required' => TRUE,
      '#validated' => TRUE,
      '#description' => $this->t('Which field will be the relation of the new images with the nodes'),
      '#states' => [
        'visible' => [
          ':input[name="content_type"]' => ['!value' => ''],
        ],
      ],
    ];

    // Field image field.
    $form['field_image'] = [
      '#type' => 'select',
      '#title' => $this->t('Image field'),
      '#suffix' => '</div>',
      '#required' => TRUE,
      '#validated' => TRUE,
      '#description' => $this->t('Where to put the new image'),
      '#states' => [
        'visible' => [
          ':input[name="content_type"]' => ['!value' => ''],
        ],
      ],
    ];

    // With the ajax of the content type, here the two previous select fields
    // will be populated.
    $contentType = $form_state->getValue('content_type');
    $fieldsSelect = [];
    if ($contentType) {
      $fields = $this->entityFieldManager->getFieldDefinitions('node', $contentType);

      $fieldsSelect = [];
      $fieldsSelect['nid'] = 'ID';

      foreach ($fields as $fieldMachineName => $field) {
        if (is_string($field->getLabel())) {
          $fieldsSelect[$fieldMachineName] = $field->getLabel();
        }
      }

      $form['node_relation']['#options'] = $fieldsSelect;
      $form['field_image']['#options'] = $fieldsSelect;
    }

    // File type, with ajax behaviour to add validator to file.
    $form['file_type'] = [
      '#type' => 'select',
      '#title' => $this->t('File extension'),
      '#description' => $this->t('File type to check images'),
      '#required' => TRUE,
      '#options' => [
        'Csv' => 'CSV',
        'Xls' => 'XLS',
        'Xlsx' => 'XLSX',
      ],
      '#description' => $this->t('Select file extension to upload'),
      '#ajax' => [
        'callback' => '::addValidatorFile',
        'event' => 'change',
        'wrapper' => 'file-upload',
        'progress' => [
          'type' => 'throbber',
          'message' => t('Checking fields...'),
        ],
      ],
    ];

    // Example file URL.
    // File Field.
    $fileExampleUrl = \Drupal::service('file_url_generator')->generateAbsoluteString(\Drupal::service('extension.list.module')->getPath('upload_node_pictures') . '/example.xlsx');

    $form['file_images'] = [
      '#type' => 'managed_file',
      '#title' => t('Select File'),
      '#size' => 22,
      '#description' => $this->t('You can check the example') . ' <a href="' . $fileExampleUrl . '" download>here</a>',
      '#upload_location' => 'public://node_pictures',
      '#prefix' => '<div id="file-upload" class="file-upload">',
      '#suffix' => '</div>',
      '#upload_validators'  => [
        'file_validate_extensions' => ['xls xlsx csv'],
      ],
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="field_image"]' => ['empty' => FALSE],
        ],
      ],
    ];

    // With the ajax in file_type, here is added the custom validator
    // Custom validator is in .module file.
    $values = $form_state->getValues();
    $inputFileType = $values['file_type'] ?? FALSE;
    $field = $values['node_relation'] ?? FALSE;

    // URL prefix if it's needed.
    $form['url_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix'),
      '#description' => $this->t('If the url needs a prefix'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Upload'),
    ];

    // Return parent::buildForm($form, $form_state);.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $excel_file = $form_state->getValue('file_images');

    $form_file = File::load($excel_file[0]);
    // Uncomment this lines to change file permanent
    // $form_file->setPermanent();
    // $form_file->save();
    $file_real_path = $this->fileSystem->realpath($form_file->getFileUri());

    $inputFileType = $values['file_type'];
    $reader = IOFactory::createReader($inputFileType);
    $reader->setReadDataOnly(TRUE);

    $spreadsheet = $reader->load($file_real_path);

    $sheetData = $spreadsheet->getActiveSheet();
    $datos = $sheetData->toArray();

    $keys = $datos[0];
    $keys = array_map('strtolower', $keys);

    $datosKeys = [];
    foreach ($datos as $i => $row) {
      if ($i > 0) {
        $arrayTemp = [];
        foreach ($row as $c => $dato) {
          if ($keys[$c] != '') {
            $arrayTemp[$keys[$c]] = $dato;
          }
        }
        $datosKeys[] = $arrayTemp;
      }
    }

    $this->uploadPictures($datosKeys, $values['node_relation'], $values['field_image'], $values['url_prefix']);
  }

  /**
   * Ajax callback para seleccion de columna para el username.
   */
  public function populateSelect(array &$form, FormStateInterface $form_state) {
    return [
      $form['node_relation'],
      $form['field_image'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function addValidatorFile(array &$form, FormStateInterface $form_state) {
    return $form['file_images'];
  }

  /**
   * {@inheritdoc}
   */
  private function uploadPictures(array $rows, $fieldRelation, $fieldImage, $urlPrefix = '') {
    $ids = [];
    foreach ($rows as $i => $row) {
      $ids[] = $row[$fieldRelation];
    }

    $nids = \Drupal::entityQuery('node')
      ->condition('nid', $ids, 'IN')
      ->accessCheck(FALSE)
      ->execute();

    $managed = TRUE;
    $destination = 'public://node_pictures/';

    $url = is_string($urlPrefix) ? $urlPrefix : '';

    $badUrls = [];
    $imported = 0;
    foreach ($rows as $i => $row) {
      $urlImage = $url . $row['url'];

      $results = \Drupal::entityQuery('node')
        ->condition($fieldRelation, $row[$fieldRelation])
        ->accessCheck(FALSE)
        ->execute();

      if (count($results)) {
        $nid = reset($results);

        $newImage = system_retrieve_file($urlImage, $destination, $managed);

        if ($newImage) {
          $node = Node::load($nid);
          $fid = $newImage->id();
          $node->$fieldImage->target_id = $fid;
          $node->save();
          $imported++;
        }
        else {
          $badUrls[] = $row[$fieldRelation];
        }
      }
    }

    if (count($badUrls)) {
      $badUrlsText = implode(',', $badUrls);
      $message = $this->t('It has been imported @count_rows rows and the ids @bad_url has not been imported', [
        '@count_rows' => $imported,
        '@bad_url' => $badUrlsText,
      ]);
    }
    else {
      $message = $this->t('It has been imported @count_rows rows.', [
        '@count_rows' => $imported,
      ]);
    }
    $this->messenger->addMessage($message);
  }

  /**
   * Get content type options.
   */
  protected function getContentTypeOptions() {
    // Get content type options.
    $types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    $options = [];
    foreach ($types as $machine_name => $type) {
      $options[$machine_name] = $type->label();
    }

    return $options;
  }

}
